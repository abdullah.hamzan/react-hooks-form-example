import React from "react";

import "./App.css";
import "./index.css";
import YoutubeForm from "./components/YoutubeForm";

function App() {
  return (
    <React.Fragment>
      <YoutubeForm />
    </React.Fragment>
  );
}

export default App;
