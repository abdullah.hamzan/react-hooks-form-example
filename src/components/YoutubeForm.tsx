import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { DevTool } from "@hookform/devtools";

let renderCount = 0;
const YoutubeForm = () => {
  renderCount += 1;
  type InputType = {
    username: string;
    email: string;
    channel: string;
  };
  const form = useForm<InputType>();
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = form;

  console.log("register:", form.getValues());
  const onsubmit: SubmitHandler<InputType> = (data: InputType) => {
    console.log("data:", data);
  };

  console.log("renderCount:", renderCount);
  return (
    <React.Fragment>
      <h1>YouTube Form ({renderCount / 2})</h1>

      <form onSubmit={handleSubmit(onsubmit)} noValidate>
        <div>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            {...register("username", { required: "Username wajib diisi" })}
          />
          <p className="error">{errors?.username?.message}</p>
        </div>
        <div>
          <label htmlFor="email">E-mail</label>
          <input
            type="email"
            id="email"
            {...register("email", {
              pattern: {
                value: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                message: "invalid email format",
              },
              validate: (fieldValidate) => {
                return (
                  fieldValidate !== "admin@example.com" ||
                  "Enter a difference email address"
                );
              },
            })}
          />
          <p className="error">{errors?.email?.message}</p>
        </div>
        <div>
          <label htmlFor="channel">Channel</label>
          <input
            type="text"
            id="channel"
            {...register("channel", {
              required: {
                value: true,
                message: "channel wajib diisi",
              },
            })}
          />
          <p className="error">{errors?.channel?.message}</p>
        </div>

        <button type="submit">Submit</button>
      </form>
      <DevTool control={control} />
    </React.Fragment>
  );
};

export default YoutubeForm;
